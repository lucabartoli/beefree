## BeeFree
A simple package to obtain BeeFree Credentials. More info at BeeFree.io.
This is a rework of the original version by ProdigyView available at https://github.com/ProdigyView/beefree
Some features had been added, as f.e.: PSR-4 loading, possibility to exclude SSL_PEER verification.

## Code Example
	use BeeFree\BeeFree;
    $beefree = new BeeFree([client_id], [client_secret]); 
    $result = $beefree -> getCredentials();
	
Optionally is possible to instance BeeFree without SSL verification

    $beefree = new BeeFree([client_id], [client_secret], false); 

After receiving credentials, call your client implementation of BeeFree Plugin and pass your token.

## Installation

	composer require lucabartoli/beefree

## License

Licensed under MIT license.
https://opensource.org/licenses/MIT