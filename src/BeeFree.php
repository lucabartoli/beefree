<?php
namespace BeeFree;

// use BeeFreeAdapter;

class BeeFree implements BeeFreeAdapter {

	//Your API Client ID
	private $_client_id = null;

	//Your API Client Secret
	private $_client_secret = null;

	//Url to call when authenicating
	private $_auth_url = 'https://auth.getbee.io/apiauth';

	//VerifySSLPeer is a curl option. Leave it unmodified unless curl SSL 
	//problems occur.
	private $_verify_ssl = true;

	/**
	 * The constructor
	 *
	 * @param string $key : The key provided by the api
	 * @param string $secret : The secret provided by the api
	 */
	public function __construct($client_id = null, $client_secret = null, $verify_ssl = null) {
		$this -> setClientID($client_id);
		$this -> setClientSecret($client_secret);
		if($verify_ssl !== null) $this -> _verify_ssl = $verify_ssl;
	}

	/**
	 * Sets the client id that is provided by the API
	 *
	 * @param string $key
	 */
	public function setClientID($client_id) {
		$this -> _client_id = $client_id;
	}

	/**
	 * Set the client secret provided by the API
	 *
	 * @param string string $secret
	 */
	public function setClientSecret($client_secret) {
		$this -> _client_secret = $client_secret;
	}

	/**
	 * Call the API and get the access token, user and other information  required
	 * to access the api
	 *
	 * @param string $grant_type : The grant type used to authenticate the API
	 * @param string $json_decode: Return the result as an object, array or raw. Default is object, to return set type to 'array'
	 *
	 * @return $mixed credentials
	 */
	public function getCredentials($grant_type = 'password', $json_decode = 'object') {
		//set POST variables
		// $fields = array('grant_type' => urlencode($grant_type), 'client_id' => urlencode($this -> _client_id), 'client_secret' => urlencode($this -> _client_secret), );
		$fields = [
			'grant_type='.urlencode($grant_type),
			'client_id='.urlencode($this -> _client_id),
			'client_secret='.urlencode($this -> _client_secret)
		];
		
		$fields_string = implode('&', $fields);

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $this->_auth_url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_SSL_VERIFYPEER => $this->_verify_ssl,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $fields_string,
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "content-type: application/x-www-form-urlencoded"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  throw new \Exception('An error occurred: '.$err);
		} 
		if($json_decode == 'array') return json_decode($response,true);
		if($json_decode == 'object') return json_decode($response);
		return $response;
	}

}
